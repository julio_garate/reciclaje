﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using DatosNegocio;

public partial class DashboardAdmin : System.Web.UI.Page
{
    public ArrayList arrayCentrosReciclaje = new ArrayList();
    public CentrosReciclaje itemCentroReciclaje = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        Usuario myUser = (Usuario)Session["usuario"];
        if (myUser == null)
        {
            this.eliminarSession();
        }
        else
        {
            this.arrayCentrosReciclaje = CentrosReciclaje.getAllCentrosReciclajeDAO();
        }
    }

    protected void onClickCloseSession(object sender, EventArgs e)
    {
        this.eliminarSession();
    }

    private void eliminarSession()
    {
        //Cerrar session
        Session.Clear();
        Response.Redirect("Default.aspx");
    }

    protected void onClickUpdate(object sender, EventArgs e)
    {
        System.Diagnostics.Debug.WriteLine("GUARDAR DATOS!!!");
    }

}
