﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"  CodeFile="Default.aspx.cs" Inherits="_Default" Title="Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<!-- Modal HTML -->
<!--div id="myModal" class="container-fluid"-->
<!-- -->
<div id="myModal" class="container-fluid">
	<div class="modal-dialog modal-login">
		<div class="modal-content">
		
			<div class="modal-header">				
				<h4 class="modal-title">Ingreso Reciclaje</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			</div>
			<div class="modal-body">
		        <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1"><i class="fa fa-user"></i></span>
                  </div>
                  <asp:TextBox id="usuario" type="text" class="form-control" placeholder="Nombre de usuario" runat="server"></asp:TextBox>
                </div>
                
		        <div class="input-group mb-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon2"><i class="fa fa-lock"></i></span>
                  </div>
                  <asp:TextBox ID="contrasenia"  type="text" class="form-control" placeholder="Contraseña" TextMode="Password" runat="server"></asp:TextBox>
                </div>
           			
				<div class="form-group">
					<!--button type="submit" class="btn btn-primary btn-block btn-lg">Ingresar</button-->
					<asp:Button ID="ButtonIngresar" runat="server" Text="Ingresar" 
                    class="btn btn-primary btn-block btn-lg" onclick="ButtonIngresar_Click" />
				</div>
				<p class="hint-text"><a href="#">¿Olvido su contraseña?</a></p>
				
			</div>
			<div class="modal-footer">¿No tiene una cuenta? <a href="#">Registrese</a></div>
		</div>
	</div>
</div>       
</div>    
             
</asp:Content>
