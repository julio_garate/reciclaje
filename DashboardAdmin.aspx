﻿<%@ Page Language="C#"MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="DashboardAdmin.aspx.cs" Inherits="DashboardAdmin" Title="Administrador" %>
<%@ Import Namespace="DatosNegocio"%>
<%@ Import Namespace="System.Collections" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<div id="myModal" class="container-fluid">

    <nav class="navbar navbar-inverse navbar-expand-xl navbar-dark" style="background: #926dde !important;">
	    <div class="navbar-header d-flex col">
		    <a class="navbar-brand" href="#"><i class="fa fa-trash-o" aria-hidden="true"></i> Reci<b>Claje</b></a>  		
		    <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle navbar-toggler ml-auto">
			    <span class="navbar-toggler-icon"></span>
			    <span class="icon-bar"></span>
			    <span class="icon-bar"></span>
			    <span class="icon-bar"></span>
		    </button>
	    </div>
	    <!-- Collection of nav links, forms, and other content for toggling -->
	    <div id="navbarCollapse" class="collapse navbar-collapse justify-content-start">		
		    <ul class="nav navbar-nav navbar-right ml-auto">
			    <li class="nav-item active"><a href="#" class="nav-link"><i class="fa fa-home"></i><span> </span><span>Centros de reciclaje</span></a></li>
			    <li class="nav-item"><a href="#" class="nav-link"><i class="fa fa-gears"></i><span> </span><span>Lugares</span></a></li>
			    <li class="nav-item"><a href="#" class="nav-link"><i class="fa fa-users"></i><span> </span><span>Usuarios</span></a></li>
			    <li class="nav-item dropdown">
				    <a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle user-action"><i class="fa fa-user-circle-o" aria-hidden="true"></i><span> </span>Administrador<b class="caret"></b></a>
				    <ul class="dropdown-menu">
					    <li><a href="#" class="dropdown-item"><i class="fa fa-user-o"></i> Perfil</a></li>
					    <li class="divider dropdown-divider"></li>
					    <li><a href="" runat="server" OnServerClick="onClickCloseSession" class="dropdown-item"><i class="fa fa-power-off" aria-hidden="true"></i> Cerrar sesión</a></li>
				    </ul>
			    </li>
		    </ul>
	    </div>
    </nav>



    <div class="container">
        <div class="table-wrapper">
            <div class="table-title">
                <div class="row">
                    <div class="col-sm-8"><h2>Centros <b>Reciclaje</b></h2></div>
                </div>
            </div>
           
            <table class="table table-striped table-hover table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nombre <i class="fa fa-sort"></i></th>
                        <th>Dirección<i class="fa fa-sort"></i></th>
                        <th>Comuna<i class="fa fa-sort"></i></th>
                        <th>Provincia<i class="fa fa-sort"></i></th>
                        <th>Región<i class="fa fa-sort"></i></th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <%foreach(CentrosReciclaje item in arrayCentrosReciclaje) { %>
                    <% string identity = "id_edit" + item.Id; %>
                        <tr>
                            <td><% HttpContext.Current.Response.Write(item.Id); %></td>
                            <td><% HttpContext.Current.Response.Write(item.Nombre); %></td>
                            <td><% HttpContext.Current.Response.Write(item.Direccion + " " + item.Calle + " " + item.Numero); %></td>
                            <td><% HttpContext.Current.Response.Write(item.Comuna); %></td>
                            <td><% HttpContext.Current.Response.Write(item.Provincia); %></td>
                            <td><% HttpContext.Current.Response.Write(item.Region); %></td>
                            <td>
							    <a href="#" data-target="#itemModalCentroReciclaje" class="view" data-toggle="modal" title="Ver" data-id="<% HttpContext.Current.Response.Write(item.getString()); %>"><i class="fa fa-eye"></i></a>
                                <a href="#" data-target="#itemModalCentroReciclajeEdit" class="edit" data-toggle="modal" title="Editar" data-id="<% HttpContext.Current.Response.Write(item.getStringEdit()); %>"><i class="fa fa-edit"></i></a>
                                
                                <a href="#" class="delete" title="Borrar" data-toggle="tooltip"><i class="fa fa-trash-o" aria-hidden="true"></i></i></a>
                            </td>
                        </tr>
                    <% } //foreach %>
                    
                </tbody>
            </table>
        </div>
    </div>  
    
    <!-- View Modal -->
    <div class="modal fade" id="itemModalCentroReciclaje" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
			<!-- style="visibility:hidden" -->
				<form>
					<div class="modal-header">						
						<h4 class="modal-title">Información de Centro de reciclaje</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">					
						<div class="form-group">
							<label>Id</label>
							<input id="itemId" type="text" class="form-control" readonly>
						</div>
						<div class="form-group">
							<label>Nombre</label>
							<input id="itemNombre" type="text" class="form-control" readonly>
						</div>
						<div class="form-group">
							<label>Telefono</label>
							<input id="itemTelefono" type="text" class="form-control" readonly>
						</div>
						<div class="form-group">
							<label>Url</label>
							<input id="itemUrl" type="text" class="form-control" readonly>
						</div>	
						<div class="form-group">
							<label>Horario Laboral en la semana</label>
							<input id="itemHorario" type="text" class="form-control" readonly>
							<label>Hora de apertura</label>
							<input id="itemHorarioApertura" type="text" class="form-control" readonly>
							<label>Hora de cierre</label>
							<input id="itemHorarioCierre" type="text" class="form-control" readonly>
						</div>
						
						<div class="form-group">
							<label>Direccion</label>
							<input id="itemDireccion" type="text" class="form-control" readonly>
							<label>Numero</label>
							<input id="itemNumero" type="text" class="form-control" readonly>
							<label>Comuna</label>
							<input id="itemComuna" type="text" class="form-control" readonly>
							<label>Provincia</label>
							<input id="itemProvincia" type="text" class="form-control" readonly>
							<label>Region</label>
							<input id="itemRegion" type="text" class="form-control" readonly>
						</div>
										
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Cerrar">
					</div>
				</form>
			</div>
		</div>
    </div>
    
    
    <!-- Edit Modal -->
    <div class="modal fade" id="itemModalCentroReciclajeEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
			<!-- style="visibility:hidden" -->
				<form>
					<div class="modal-header">						
						<h4 class="modal-title">Información de Centro de reciclaje</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">					
						<div class="form-group">
							<label>Id</label>
							<input id="editId" type="text" class="form-control" readonly>
						</div>
						<div class="form-group">
							<label>Nombre</label>
							<asp:TextBox runat="server" id="editNombre" type="text" class="form-control"></asp:TextBox>
						</div>
						<div class="form-group">
							<label>Telefono</label>
							<input runat="server" id="editTelefono" type="text" class="form-control" required>
						</div>
						<div class="form-group">
							<label>Url</label>
							<input runat="server" id="editUrl" type="text" class="form-control" required>
						</div>	
						<div class="form-group">
							<label>Horario Laboral en la semana</label>
							<input runat="server" id="editHorario" type="text" class="form-control" required>
							<label>Hora de apertura</label>
							<input runat="server" id="editHorarioApertura" type="text" class="form-control" required>
							<label>Hora de cierre</label>
							<input runat="server" id="editHorarioCierre" type="text" class="form-control" required>
						</div>
						
						<div class="form-group">
							<label>Direccion</label>
							<input runat="server" id="editDireccion" type="text" class="form-control" required>
							<label>Numero</label>
							<input runat="server" id="editNumero" type="text" class="form-control" required>
							<label>Comuna</label>
							<input runat="server" id="editComuna" type="text" class="form-control" readonly>
							<label>Provincia</label>
							<input runat="server"  id="editProvincia" type="text" class="form-control" readonly>
							<label>Region</label>
							<input runat="server"  id="editRegion" type="text" class="form-control" readonly>
						</div>
										
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancelar">
						
						<asp:Button id="buttonGuardarCambios" runat="server" onclick="onClickUpdate"  type="submit" class="btn btn-info" Text="Guardar"/>
					</div>
				</form>
			</div>
		</div>
    </div>


    	<!-- Delete Employee Delete Modal HTML -->
	<div id="deleteEmployeeModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<form>
					<div class="modal-header">						
						<h4 class="modal-title">Borrar centor de reciclaje</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">					
						<p>¿Esta seguro que quiere borrar el registro?</p>
						<p class="text-warning"><small>Esta acción no se puede deshacer.</small></p>
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
						<input type="submit" class="btn btn-danger" value="Delete">
					</div>
				</form>
			</div>
		</div>
	</div>

</div>




</asp:Content>
