﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using DataBase;

namespace DatosNegocio
{
    /// <summary>
    /// Summary description for Negocio
    /// </summary>
    public class Usuario
    {
        private int id;

        private string username;

        private string contrasenia;

        private string nombre;

        private string apellido;

        private string telefono;

        private string correo;

        private int idTipoUsuario;

        private int idDireccion;

        public Usuario()
        {

        }

        public Usuario(int id, string username, string contrasenia, string nombre,
            string apellido, string telefono, string correo, int idTipoUsuario, int idDireccion)
        {
            this.id = id;
            this.username = username;
            this.contrasenia = contrasenia;
            this.nombre = nombre;
            this.apellido = apellido;
            this.telefono = telefono;
            this.correo = correo;
            this.idTipoUsuario = idTipoUsuario;
            this.idDireccion = idDireccion;
        }

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Username
        {
            get { return username; }
            set { username = value; }
        }

        public string Contrasenia
        {
            get { return contrasenia; }
            set { contrasenia = value; }
        }

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        public string Apellido
        {
            get { return apellido; }
            set { apellido = value; }
        }

        public string Telefono
        {
            get { return telefono; }
            set { telefono = value; }
        }

        public string Correo
        {
            get { return correo; }
            set { correo = value; }
        }

        public int IdTipoUsuario
        {
            get { return idTipoUsuario; }
            set { idTipoUsuario = value; }
        }

        public int IdDireccion
        {
            get { return idDireccion; }
            set { idDireccion = value; }
        }

        public static Usuario getUsuarioDAO(string username, string contrasenia)
        {
            UsuarioDAO usuarioDAO = new UsuarioDAO();
            return usuarioDAO.BuscarUsuario(username, contrasenia);
        }

    }

    public class CentrosReciclaje
    {
        private int id;
        private string nombre;
        private string telefono;
        private string url;
        private int idDireccion;
        private int idHorario;

        //Valores que de otras tablas
        //Para horarios
        private String dias;
        private String horaApertura;
        private String horaCierre;

        //Para direccion
        private string direccion;
        private string calle;
        private string numero;
        private string otro;

        private string comuna;
        private string provincia;
        private string region;



        public CentrosReciclaje()
        {
        }

        public CentrosReciclaje(int id, string nombre, string telefono, string url, int idDireccion, int idHorario)
        {
            this.id = id;
            this.nombre = nombre;
            this.telefono = telefono;
            this.url = url;
            this.idDireccion = idDireccion;
            this.idHorario = idHorario;
        }

        public static ArrayList getAllCentrosReciclajeDAO()
        {
            CentroReciclajeDAO dao = new CentroReciclajeDAO();
            return dao.getAllCentrosReciclaje();
        }

        public string getString()
        {
            string json =
                "{" +
                    " 'id': " + this.id + "," +
                    " 'nombre': '" + this.nombre + "'," +
                    " 'telefono': '" + this.telefono + "'," +
                    " 'url': '" + this.url + "'," +
                    " 'dias': '" + this.dias + "'," +
                    " 'hora_apertura': '" + this.horaApertura + "'," +
                    " 'hora_cierre': '" + this.HoraCierre + "'," +
                    " 'direccion': '" + this.direccion + "'," +
                //" 'calle': '" + this.calle + "'," +
                    " 'numero':'" + this.numero + "'," +
                    " 'comuna':'" + this.comuna + "'," +
                    " 'provincia':'" + this.provincia + "'," +
                    " 'region':'" + this.region + "'" +
                "}";
            return json;
        }

        public string getStringEdit()
        {
            //Obtener el arreglo de Regiones
            UtilitarioDAO utilDAO = new UtilitarioDAO();
            ArrayList arrayRegiones = utilDAO.getAllRegionProvinciaComunas();
            string jsonRegiones = "[";
            for (int i = 0; i < arrayRegiones.Count; i++)
            {
                Region r = (Region)arrayRegiones[i];
                jsonRegiones += r.getJson();
                if (i < arrayRegiones.Count -1)
                {
                    jsonRegiones += ",";
                }
            }
            jsonRegiones += "]";

            string json =
                "{" +
                    " 'id': " + this.id + "," +
                    " 'nombre': '" + this.nombre + "'," +
                    " 'telefono': '" + this.telefono + "'," +
                    " 'url': '" + this.url + "'," +
                    " 'dias': '" + this.dias + "'," +
                    " 'hora_apertura': '" + this.horaApertura + "'," +
                    " 'hora_cierre': '" + this.HoraCierre + "'," +
                    " 'direccion': '" + this.direccion + "'," +
                    " 'numero':'" + this.numero + "'," +
                    " 'comuna':'" + this.comuna + "'," +
                    " 'provincia':'" + this.provincia + "'," +
                    " 'region':'" + this.region + "'," +
                    " 'utilitario': " + jsonRegiones +
                "}";
            return json;
        }

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public string Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        public string Telefono
        {
            get { return telefono; }
            set { telefono = value; }
        }

        public string Url
        {
            get { return url; }
            set { url = value; }
        }

        public String Dias
        {
            get { return dias; }
            set { dias = value; }
        }

        public String HoraApertura
        {
            get { return horaApertura; }
            set { horaApertura = value; }
        }

        public String HoraCierre
        {
            get { return horaCierre; }
            set { horaCierre = value; }
        }

        public int IdDireccion
        {
            get { return idDireccion; }
            set { idDireccion = value; }
        }

        public int IdHorario
        {
            get { return idHorario; }
            set { idHorario = value; }
        }

        public string Direccion
        {
            get { return direccion; }
            set { direccion = value; }
        }

        public string Calle
        {
            get { return calle; }
            set { calle = value; }
        }

        public string Numero
        {
            get { return numero; }
            set { numero = value; }
        }

        public string Otro
        {
            get { return otro; }
            set { otro = value; }
        }

        public string Comuna
        {
            get { return comuna; }
            set { comuna = value; }
        }

        public string Provincia
        {
            get { return provincia; }
            set { provincia = value; }
        }

        public string Region
        {
            get { return region; }
            set { region = value; }
        }

    }

    public class Horario
    {
        private int id;

        public int Id
        {
            get { return id; }
            set { id = value; }
        }
        private String dias;

        public String Dias
        {
            get { return dias; }
            set { dias = value; }
        }
        private String horaApertura;

        public String HoraApertura
        {
            get { return horaApertura; }
            set { horaApertura = value; }
        }
        private String horaCierre;

        public String HoraCierre
        {
            get { return horaCierre; }
            set { horaCierre = value; }
        }
    }

    public class Region
    {
        private int id;
        private String nombre;
        private ArrayList provincias = new ArrayList();

        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        public String Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }

        public ArrayList Provincias
        {
            get { return provincias; }
            set { provincias = value; }
        }

        public string getJson()
        {
            string jsonProvincias = "[";
            for (int i = 0; i < Provincias.Count; i++)
            {
                Provincia p = (Provincia)Provincias[i];
                jsonProvincias += p.getJson();
                if (i < Provincias.Count - 1)
                {
                    jsonProvincias += ",";
                }
            }

            jsonProvincias += "]";

            string json =
                "{" +
                    " 'id': " + this.id + "," +
                    " 'nombre': '" + this.nombre + "'," +
                    " 'arrayProvincias': " + jsonProvincias +
                "}";
            return json;
        }
    }

        public class Provincia
        {
            private int id;
            private String nombre;
            private ArrayList comunas = new ArrayList();

            public int Id
            {
                get { return id; }
                set { id = value; }
            }

            public String Nombre
            {
                get { return nombre; }
                set { nombre = value; }
            }

            public ArrayList Comunas
            {
                get { return comunas; }
                set { comunas = value; }
            }

            public string getJson()
            {
                string jsonComunas = "[";
                for (int i = 0; i < this.Comunas.Count; i++)
                {
                    Comuna c = (Comuna)Comunas[i];
                    jsonComunas += c.getJson();
                    if (i < this.Comunas.Count - 1)
                    {
                        jsonComunas += ",";
                    }
                }

                jsonComunas += "]";

                string json =
                    "{" +
                        " 'id': " + this.id + "," +
                        " 'nombre': '" + this.nombre + "'," +
                        " 'arrayComunas': " + jsonComunas +
                    "}";
                return json;
            }
        }

        public class Comuna
        {
            private int id;
            private String nombre;

            public int Id
            {
                get { return id; }
                set { id = value; }
            }

            public String Nombre
            {
                get { return nombre; }
                set { nombre = value; }
            }

            public string getJson()
            {
                string json =
                    "{" +
                        " 'id': " + this.id + "," +
                        " 'nombre': '" + this.nombre + "'" +
                    "}";
                return json;
            }
        }
    }
