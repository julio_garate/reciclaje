﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.OleDb;
using System.Collections;
using DatosNegocio;

namespace DataBase
{
    /// <summary>
    /// Summary description for DataBase
    /// </summary>
    public class DataBase
    {
        public static String connectionString = "Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" +
                                   HttpContext.Current.Server.MapPath(@"Datos\Reciclaje-2000.mdb") + ";";
        public DataBase()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public static DataSet ExecuteQuery(String consulta)
        {
            System.Diagnostics.Debug.WriteLine(consulta);
            OleDbConnection cnn = new OleDbConnection(connectionString);
            OleDbDataAdapter da = new OleDbDataAdapter(consulta, cnn);
            DataSet ds = new DataSet();
            try
            {
                cnn.Open();
                da.Fill(ds);
            }
            catch (OleDbException ex)
            {
                System.Diagnostics.Debug.WriteLine("Error en BD");
                ds = null;
            }
            finally
            {
                cnn.Close();
            }
            return ds;
        } // Fin LeeArchivoAccessUsandoConexionOleDb()
    }

    public class UsuarioDAO{
    
        public Usuario BuscarUsuario(string usuario, string password){
            string sql = "SELECT * FROM usuario WHERE username='"+ usuario + "' and contrasenia='" + password + "'";
            DataSet ds = DataBase.ExecuteQuery(sql);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Usuario user = new Usuario();
                user.Id =  Convert.ToInt32(ds.Tables[0].Rows[0]["ID"]);
                user.Username = ds.Tables[0].Rows[0]["USERNAME"].ToString();
                user.Contrasenia = ds.Tables[0].Rows[0]["CONTRASENIA"].ToString();
                user.Nombre = ds.Tables[0].Rows[0]["NOMBRE"].ToString();
                user.Apellido = ds.Tables[0].Rows[0]["APELLIDO"].ToString();
                user.Correo = ds.Tables[0].Rows[0]["CORREO"].ToString();
                user.IdTipoUsuario = Convert.ToInt32(ds.Tables[0].Rows[0]["TIPO_USUARIO"]);
                if (ds.Tables[0].Rows[0]["ID_DIRECCION"] != DBNull.Value)
                {
                    user.IdDireccion = Convert.ToInt32(ds.Tables[0].Rows[0]["ID_DIRECCION"]);
                }
                return user;
            }
            
            return null;
        }
    }

    public class CentroReciclajeDAO
    {
        public ArrayList getAllCentrosReciclaje()
        {
            string sql = "SELECT cr.Id, cr.Nombre, cr.telefono, cr.url, h.dia as dias, " + 
                            "h.hora_apertura, h.hora_cierre, d.direccion, d.calle, d.numero, " + 
                            "c.Nombre AS comuna, p.Nombre AS Provincia, r.Nombre as region, d.Id as id_direccion, " +
                            "h.Id as id_horario " +
                            "FROM ((((centro_reciclaje AS cr INNER JOIN direccion AS d ON cr.id_direccion = d.Id) " +
                            "INNER JOIN comuna AS c ON c.Id = d.id_comuna) INNER JOIN Provincia AS p ON p.Id = c.id_provincia) " +
                            "INNER JOIN Region as r ON r.Id = p.id_region) INNER JOIN horario as h ON h.Id = cr.id_horario";
            DataSet ds = DataBase.ExecuteQuery(sql);
            ArrayList myArryList = null;
            if (ds.Tables[0].Rows.Count > 0)
            {
                myArryList = new ArrayList();
                foreach (DataTable table in ds.Tables)
                {
                    foreach (DataRow dr in table.Rows)
                    {
                        CentrosReciclaje cr = new CentrosReciclaje();
                        cr.Id = Convert.ToInt32(dr["ID"]);
                        cr.Nombre = dr["NOMBRE"].ToString();
                        cr.Telefono = dr["TELEFONO"].ToString();
                        cr.Url = dr["URL"].ToString();
                        cr.Dias = dr["DIAS"].ToString();
                        cr.HoraApertura = dr["HORA_APERTURA"].ToString();
                        cr.HoraCierre = dr["HORA_CIERRE"].ToString();
                        cr.Direccion = dr["DIRECCION"].ToString();
                        cr.Calle = dr["CALLE"].ToString();
                        cr.Numero = dr["NUMERO"].ToString();
                        cr.Comuna = dr["COMUNA"].ToString();
                        cr.Provincia = dr["PROVINCIA"].ToString();
                        cr.Region = dr["REGION"].ToString();
                        cr.IdDireccion = Convert.ToInt32(dr["ID_DIRECCION"]);
                        cr.IdHorario = Convert.ToInt32(dr["ID_HORARIO"]);
                        myArryList.Add(cr);
                        System.Diagnostics.Debug.WriteLine(dr);
                    }
                }
            }
            return myArryList;
        }

    }

    public class UtilitarioDAO
    {
        public ArrayList getAllRegionProvinciaComunas()
        {
            ArrayList myArryList = null;
            string sql = "SELECT r.Id as id_region, r.Nombre as region_nombre, p.Id as id_provincia, " + 
                "p.Nombre as provincia_nombre, c.Id as id_comuna, c.Nombre as comuna_nombre " + 
                "FROM (Region as r INNER JOIN Provincia as p ON p.id_region = r.Id) " +
                "INNER JOIN Comuna as c ON c.id_provincia = p.Id ORDER BY r.Id ASC";

            sql = "SELECT r.id, r.nombre FROM region as r";

            
            DataSet ds = DataBase.ExecuteQuery(sql);
            if (ds.Tables[0].Rows.Count > 0)
            {
                myArryList = new ArrayList();
                foreach (DataTable table in ds.Tables)
                {
                    foreach (DataRow dr in table.Rows)
                    {
                        Region r = new Region();
                        r.Id = Convert.ToInt32(dr["ID"]);
                        r.Nombre = dr["NOMBRE"].ToString();
                        //Llamamos las provincias
                        String sqlProvincias = 
                            "SELECT p.id, p.nombre " + 
                            "FROM provincia as p " + 
                            "WHERE p.id_region = " + r.Id;
                        DataSet dsProvincias = DataBase.ExecuteQuery(sqlProvincias);
                        if (dsProvincias.Tables[0].Rows.Count > 0)
                        {
                            foreach (DataTable tableP in dsProvincias.Tables)
                            {
                                foreach (DataRow drP in tableP.Rows)
                                {
                                    Provincia p = new Provincia();
                                    p.Id = Convert.ToInt32(drP["ID"]);
                                    p.Nombre = drP["NOMBRE"].ToString();
                                    //Llamamos a las comunas asociado a su provincia
                                    String sqlComunas =
                                        "SELECT c.id, c.nombre " +
                                        "FROM comuna as c " +
                                        "WHERE c.id_provincia = " + p.Id;
                                    if (p.Id == 5)
                                    {
                                        System.Diagnostics.Debug.WriteLine("Estamos en Santiago");
                                    }
                                    DataSet dsComunas = DataBase.ExecuteQuery(sqlComunas);
                                    if (dsComunas.Tables[0].Rows.Count > 0)
                                    {
                                        foreach (DataTable tableC in dsComunas.Tables)
                                        {
                                            foreach (DataRow drC in tableC.Rows)
                                            {
                                                Comuna c = new Comuna();
                                                c.Id = Convert.ToInt32(drC["ID"]);
                                                c.Nombre = drC["NOMBRE"].ToString();
                                                p.Comunas.Add(c);
                                            }
                                        }
                                    }
                                    r.Provincias.Add(p);
                                }
                            }
                        }
                        myArryList.Add(r);
                    }
                }
            }

            return myArryList;
        }
    }
}
