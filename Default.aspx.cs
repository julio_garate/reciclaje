﻿using System;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using DatosNegocio;

public partial class _Default : System.Web.UI.Page 
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session.Clear();
    }

    protected void ButtonIngresar_Click(object sender, EventArgs e)
    {
        System.Diagnostics.Debug.WriteLine(this.usuario.Text + " " + this.contrasenia.Text);

        if (this.usuario.Text != "" && this.contrasenia.Text != "")
        {
            System.Diagnostics.Debug.WriteLine("Datos no son vacios");
            Usuario myUser = Usuario.getUsuarioDAO(this.usuario.Text, this.contrasenia.Text);

            if (myUser != null)
            {
                Session["usuario"] = myUser;
                if (myUser.IdTipoUsuario == 1)
                {
                    //Administrador
                    Response.Redirect("DashboardAdmin.aspx");
                }
                else
                {
                    //Cliente
                    Response.Redirect("DashboardAdmin.aspx");
                }
            }
            else
            {
            }
        }
        else
        {
            System.Diagnostics.Debug.WriteLine("Datos nulos");
        }
        
    }
}
